package com.verizon.oi;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by piyush on 10/29/15.
 */

public class Dweet{

    private static String url_send = "https://dweet.io/dweet/for/early-stamp-send";
    private static String url_get = "https://dweet.io/get/latest/dweet/for/early-stamp";
    public static JSONObject content;

    private static String TAG = "dweet";

    public static void send(String mRequest, Context context) {
        Log.i(TAG, mRequest);
        JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.POST, url_send, mRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // the response is already constructed as a JSONObject
                        Log.i(TAG, response.toString());
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        Volley.newRequestQueue(context).add(jsonRequest);

    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json; charset=utf-8");
        return headers;
    }

    public static void get(Context context, final VolleyCallback callback) {
        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url_get, (String) null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray j = response.getJSONArray("with");
                    content = new JSONObject(j.get(0).toString()).getJSONObject("content");
                    Log.i(TAG, content.toString());
                    callback.onSuccess(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }
        );
        queue.add(jsObjRequest);
    }

 /*   public static String getKey(String key, Context context) {
        get(context);
        Log.i("TAG", content.toString());
        try {
            return content.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return "error";
        }
    }*/

    public interface VolleyCallback{
        void onSuccess(JSONObject result);
    }

}
